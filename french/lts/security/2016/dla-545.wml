#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été identifiés et corrigés dans ICU,
la bibliothèque C et C++ « International Components for Unicode », dans
Debian Wheezy.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-2632">CVE-2015-2632</a>

<p>Vulnérabilité de dépassement de tampon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-4844">CVE-2015-4844</a>

<p>Vulnérabilité de dépassement de tampon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0494">CVE-2016-0494</a>

<p>Vulnérabilité d'entier signé et de dépassement d'entier.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.8.1.1-12+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets icu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-545.data"
# $Id: $

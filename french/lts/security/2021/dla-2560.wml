#use wml::debian::translation-check translation="9ac65e6a0fe9be4e6a68860c7ae77cbede2d0a46" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans QEMU, un émulateur rapide
de processeur (utilisé notamment dans KVM et la virtualisation HVM Xen). Un
attaquant pouvait déclencher un déni de service (DoS), une fuite d'informations
et éventuellement exécuter du code arbitraire avec les privilèges du processus
QEMU sur l’hôte.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15469">CVE-2020-15469</a>

<p>Un objet MemoryRegionOps pouvait ne pas avoir de méthodes de rappel
d’écriture ou lecture, conduisant à un déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15859">CVE-2020-15859</a>

<p>QEMU était sujet à une utilisation de mémoire après libération dans
hw/net/e1000e_core.c à cause d’un utilisateur de système invité pouvant
déclencher un paquet e1000e avec l’adresse de données réglée à l’adresse
MMIO de e1000e.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25084">CVE-2020-25084</a>

<p>QEMU était sujet à une utilisation de mémoire après libération dans
hw/usb/hcd-xhci.c parce que la valeur de retour usb_packet_map n’était pas
vérifiée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28916">CVE-2020-28916</a>

<p>hw/net/e1000e_core.c était sujet à une boucle infinie à l'aide d'un
descripteur RX avec une adresse de tampon NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29130">CVE-2020-29130</a>

<p>slirp.c était sujet à une lecture excessive de tampon car il essayait de
lire un certain montant de données d’en-tête même si cela excédait la longueur
totale de paquet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29443">CVE-2020-29443</a>

<p>ide_atapi_cmd_reply_end dans hw/ide/atapi.c permettait un accès en lecture
hors limites à cause d’un indice de tampon non validé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20181">CVE-2021-20181</a>

<p>9pfs : ZDI-CAN-10904 : vulnérabilité TOCTOU d’élévation de privilèges dans le
système de fichiers Plan 9 de QEMU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20221">CVE-2021-20221</a>

<p>aarch64 : GIC : accès hors limites dans le tampon de tas à l'aide d'un champ
ID interrompu.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:2.8+dfsg-6+deb9u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2560.data"
# $Id: $

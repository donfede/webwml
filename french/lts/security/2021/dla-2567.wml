#use wml::debian::translation-check translation="96cfe8a78677ff41aaaf35cefbe4e9bdf06f1ea5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans unrar-free, un extracteur de
fichiers .rar.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14120">CVE-2017-14120</a>

<p>Ce CVE est relatif à une vulnérabilité de traversée de répertoires pour les
archives RAR version 2.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14121">CVE-2017-14121</a>

<p>Ce CVE est relatif à un défaut de déréférencement de pointeur NULL déclenché
par une archive RAR contrefaite pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14122">CVE-2017-14122</a>

<p>Ce CVE est relatif à une lecture hors limites de tampon de pile.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:0.0.1+cvs20140707-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unrar-free.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de unrar-free, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/unrar-free">\
https://security-tracker.debian.org/tracker/unrar-free</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2567.data"
# $Id: $

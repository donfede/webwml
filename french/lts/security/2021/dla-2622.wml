#use wml::debian::translation-check translation="b57c1e1bc5989d37c0cf58fb21fae9b5add6a3ef" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème potentiel de traversée de
répertoires dans Django, un cadriciel de développement web basé sur Python.</p>

<p>La vulnérabilité pouvait avoir été exploitée à l’aide de noms de fichier
contrefaits de manière malveillante. Cependant, les gestionnaires de
téléchargement construits dans Django lui-même n’étaient pas affectés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28658">CVE-2021-28658</a>

<p>Dans Django, versions 2.2 avant la version 2.2.20, versions 3.0 avant la
version 3.0.14 et versions 3.1 avant la version 3.1.8, MultiPartParser
permettait une traversée de répertoires à l’aide des fichiers téléchargés avec
des noms de fichier contrefaits de manière adaptée. Les gestionnaires internes
n’étaient pas affectés par cette vulnérabilité.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.10.7-2+deb9u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2622.data"
# $Id: $

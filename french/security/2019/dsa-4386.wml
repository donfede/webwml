#use wml::debian::translation-check translation="399626927b999b3938582bfb0243645dfed48f14" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans cURL, une bibliothèque
de transfert par URL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16890">CVE-2018-16890</a>

<p>Wenxiang Qian de Tencent Blade Team a découvert que la fonction
traitant les messages NTLM type-2 entrants ne valide pas correctement les
données entrantes et est sujette à une vulnérabilité de dépassement
d'entier qui pourrait conduire à une lecture de tampon hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3822">CVE-2019-3822</a>

<p>Wenxiang Qian de Tencent Blade Team a découvert que la fonction créant
un en-tête NTLM type-3 sortant est sujette à une vulnérabilité de
dépassement d'entier qui pourrait conduire à une écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3823">CVE-2019-3823</a>

<p>Brian Carpenter de Geeknik Labs a découvert que le code traitant la fin
de réponse pour SMTP est sujet à une lecture de tas hors limites.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 7.52.1-5+deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4386.data"
# $Id: $

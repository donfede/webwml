#use wml::debian::template title="Cantos dos(as) novos(as) membros(as) do Debian" BARETITLE="true"
#use wml::debian::translation-check translation="e75c4ef4f01457261f11a81e80de9862be35be30"

<p>O processo para novo(a) membro(a) (NM) do Debian é o processo para se tornar
um(a) desenvolvedor(a) oficial do Debian (DD).
Essas páginas web são os locais onde os(as) futuros(as) desenvolvedores(as)
Debian podem encontrar todos os detalhes sobre como se candidatar para se
tornar um(a) DD, as diferentes etapas do processo e como acompanhar o andamento
da sua candidatura.</p>

<p>O primeiro ponto importante a ser destacado é que você <em>não</em> precisa
ser um(a) desenvolvedor(a) oficial do Debian para ajudar a melhorar o Debian.
Na verdade, você já deve ter um histórico de contribuições anteriores ao Debian
antes de se candidatar ao processo de novos(as) membros(as).</p>

<p><a name="non-maintainer-contributions"></a>O Debian é uma comunidade aberta
e dá boas-vindas a todas as pessoas que desejam usar ou ajudar a melhorar nossa
distribuição. Como não desenvolvedor(a), você pode:</p>

<ul>
  <li>manter pacotes através de um(a) <a href="#Sponsor">padrinho/madrinha.</a></li>
  <li>criar e/ou revisar traduções.</li>
  <li>criar ou melhorar documentação.</li>
  <li><a href="../website">ajudar a manter o site web.</a></li>
  <li>ajudar no tratamento de bugs (fornecendo patches, registrando bons bugs,
      confirmando a existência do bug, encontrando maneiras de reproduzir o
      problema, ...).</li>
  <li>ser um(a) membro(a) ativo(a) de uma equipe de empacotamento (por exemplo,
      do debian-qt-kde ou do debian-gnome.)</li>
  <li>ser um(a) membro(a) ativo(a) de um subprojeto (por exemplo do
      debian-installer ou do debian-desktop).</li>
  <li>etc.</li>
</ul>

<p>A <a href="$(DOC)/developers-reference/new-maintainer.html">referência do(a)
desenvolvedor(a) Debian</a> contém várias sugestões concretas sobre como
realizar várias dessas tarefas (em particular, como encontrar
padrinhos/madrinhas disponíveis).</p>

<p>O processo para novo(a) membro(a) do Debian é o processo para se tornar um(a)
desenvolvedor(a) oficial Debian (DD). Este é o papel tradicional de associação
plena no Debian. Um(a) DD pode participar das eleições do Debian.
O DD <em>uploading</em> pode fazer upload de qualquer
pacote para o repositório. Antes de aplicar para ser um(a) DD <em>uploading</em>,
você deve ter um histórico de manutenção de pacotes por pelo menos seis meses.
Por exemplo, fazendo upload de pacotes como um(a)
<a href="https://wiki.debian.org/DebianMaintainer">mantenedor(a) Debian (DM)</a>,
trabalhando dentro de uma equipe ou mantendo pacotes enviados por
padrinhos/madrinhas. DDs <em>non-uploading</em> têm as mesmas permissões no
repositório que os(as) mantenedores(as) Debian. Antes de aplicar como
DD <em>non-uploading</em>, você deve ter um histórico visível e significativo
de trabalhos dentro do projeto.</p>

<p>É importante entender que o processo para novo(a) membro(a) faz parte dos
esforços de garantia de qualidade do Debian. É difícil encontrar
desenvolvedores(as) que possam gastar tempo suficiente em suas tarefas do
Debian, por isso achamos importante verificar se o(a) candidato(a) é
capaz de sustentar seu trabalho e fazê-lo bem. Portanto, exigimos que os(as)
futuros(as) membros(as) já estejam ativamente envolvidos(as) há algum tempo no
Debian.</p>

<p><a name="developer-priveleges"></a>Todo(a) desenvolvedor(a) Debian:</p>
<ul>
  <li>é membro(a) do projeto Debian;</li>
  <li>tem permissão para votar sobre questões relacionadas a todo o projeto;</li>
  <li>pode fazer login na maioria dos sistemas que mantêm o Debian em execução;</li>
  <li>tem permissão para fazer upload de <em>todos</em> os pacotes
      (exceto desenvolvedores(as) <em>non-uploading</em> que têm os direitos de
      upload de um(a) DM);</li>
  <li>tem acesso a lista de discussão debian-private.</li>
</ul>

<p>Em outras palavras, tornar-se um(a) desenvolvedor(a) Debian concede a você
vários privilégios importantes em relação à infraestrutura do projeto.
Obviamente, isso requer muita confiança e comprometimento do(a)
candidato(a).</p>

<p>Consequentemente, todo o processo de NM é muito rigoroso e completo. Isso
não significa desencorajar as pessoas interessadas em se tornar um(a)
desenvolvedor(a) registrado(a), mas explica porque o processo de novos(as)
membros(as) leva tanto tempo.</p>

<p>Por favor leia o <a href="#Glossary">glossário de definições</a> antes de
ler o restante das páginas.</p>

<p>As páginas seguintes são de interesse do(a) candidato(a):</p>

<ul>
 <li><a href="nm-checklist">Lista das etapas necessárias para o(a) candidato(a)</a>
  <ul>
   <li><a href="nm-step1">Etapa 1: candidatura</a></li>
   <li><a href="nm-step2">Etapa 2: identificação</a></li>
   <li><a href="nm-step3">Etapa 3: filosofia e procedimentos</a></li>
   <li><a href="nm-step4">Etapa 4: tarefas e habilidades</a></li>
   <li><a href="nm-step5">Etapa 5: recomendação</a></li>
   <li><a href="nm-step6">Etapa 6: verificação da secretaria</a></li>
   <li><a href="nm-step7">Etapa 7: verificação do(a) gestor(a) de contas Debian
    e criação de conta</a></li>
  </ul></li>
 <li><a href="https://nm.debian.org/public/newnm">Formulário de inscrição</a></li>
</ul>

<p>Se você é um(a) desenvolvedor(a) Debian e está interessado(a) em participar
do processo para novos(as) membros(as), por favor visite estas páginas: </p>
<ul>
  <li><a href="nm-amchecklist">Checklist para gestores(as) de candidaturas</a></li>
  <li><a href="nm-advocate">Advogando a favor de um(a) futuro(a) membro(a)</a></li>
  <li><a href="nm-amhowto">MiniHOWTO para gestores(as) de candidaturas</a></li>
  <li><a href="$(HOME)/events/keysigning">MiniHOWTO para assinatura de chave</a></li>
</ul>

<p>Diversos:</p>
<ul>
  <li><a href="https://nm.debian.org/">Banco de dados com o estado do processo
      para novo(a) membro(a)</a></li>
  <li><a href="https://nm.debian.org/process/">Lista atualizada de
      candidatos(as)</a></li>
  <li><a href="https://nm.debian.org/public/managers">Lista atualiza de
      gestores(as) de candidaturas</a></li>
</ul>

<define-tag email>&lt;<a href="mailto:%0">%0</a>&gt;</define-tag>

<h2><a name="Glossary">Glossário de definições</a></h2>
<dl>
 <dt><a name="Advocate">Advogado (<em>Advocate</em>)</a>:</dt>
  <dd>Um(a) <a href="#Member">membro(a) do Debian</a> que endossa um(a)
   candidato(a). Ele(a) deve conhecer o(a) <a href="#Applicant">candidato(a)</a>
   razoavelmente bem e deve ser capaz de fornecer uma visão geral do trabalho,
   interesses e planos do(a) candidato(a). Os(As) advogados(as) geralmente são
   os <a href="#Sponsor">padrinhos/madrinhas</a> de um(a) candidato(a).
   Observação: "advogado(a)" no contexto do Debian não é o(a) profissional da
   advocacia, mas alguém que "advoga" a favor de um(a) candidato(a) de maneira
   não profissional.   
  </dd>

 <dt><a name="Applicant">Candidato(a) (<em>Applicant</em>)</a>, novo(a) membro(a) (NM - <em>New Member</em>), historicamente conhecido(a) como novo(a) mantenedor(a):</dt>
  <dd>Uma pessoa solicitando associação ao Debian como desenvolvedor(a)
   Debian.</dd>

 <dt><a name="AppMan">Gestor(a) de candidatura (AM - <em>Application Manager</em>)</a>:</dt>
  <dd>Um(a) <a href="#Member">membro(a) do Debian</a> que é designado(a) para
   um(a) <a href="#Applicant">candidato(a)</a> para coletar as informações
   necessárias pelos(as) <a href="#DAM">gestores(as) de contas Debian
   (<em>Debian Account Manager</em>)</a> para decidir sobre uma candidatura.
   Um(a) gestor(a) de candidatura pode ser designado(a) a mais de um(a)
   candidato(a).</dd>

 <dt><a name="FrontDesk">Secretaria (<em>Front Desk</em>)</a>: <email nm@debian.org></dt>
  <dd>Os(As) membros(as) da secretaria fazem o trabalho de infraestrutura do
   processo de NM, como o recebimento inicial das candidaturas, das mensagens
   de endosso e dos relatórios finais da candidatura, e a designação de AMs para
   os NMs. Eles(as) são o ponto de contato se surgirem problemas com a
   candidatura.</dd>

 <dt><a name="Member">Membro(a), Desenvolvedor(a) (<em>Member, Developer</em>)</a>:</dt>
  <dd>Um(a) membro(a) do Debian, que passou pelo processo para novo(a) membro(a)
    e teve sua candidatura aceita.</dd>

 <dt><a name="Sponsor">Padrinho/Madrinha (<em>Sponsor</em>)</a>:</dt>
  <dd>Um(a) <a href="#Member">membro(a) do Debian</a> que atua como mentor(a)
   de um(a) candidato(a): Ele(a) verifica os pacotes fornecidos pelo(a)
   candidato(a) e ajuda a encontrar problemas e a melhorar o empacotamento.
   Quando o padrinho/madrinha está satisfeito com o pacote, ele(a) faz o upload
   em prol do(a) candidato(a) para o repositório do Debian. O(A) candidato(a) é
   registrado(a) como o(a) mantenedor(a) desse pacote, mesmo apesar do fato
   dele(a) não fazer uploads de pacotes.</dd>
</dl>

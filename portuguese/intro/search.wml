#use wml::debian::template title="Informações sobre como usar o mecanismo de busca do Debian"
#use wml::debian::translation-check translation="821d2af3a565be7b911813a3fb1a5543be4391e6" maintainer="Thiago Pezzo (Tico)"

<P>O mecanismo de busca do Debian em <a href="https://search.debian.org/">https://search.debian.org/</a>
permite diferentes tipos de pesquisa, dependendo do que você quiser fazer.

<H3>Busca Simples</H3>

<P>O jeito mais simples de todos é digitar uma palavra na caixa de pesquisa
e pressionar enter (ou clicar no botão <em>Procurar</em>). O mecanismo de busca
vai então retornar todas as páginas do website que contenham aquela palavra.
Esse tipo de busca dá bons resultados na maioria das vezes.

<P>O próximo passo é pesquisar por mais de uma palavra ao mesmo tempo, o que
retornará uma lista de páginas do website que contêm todas as palavras que você
digitou.

<H3>Busca booleana</H3>

<P>Se uma busca simples não é suficiente, então uma pesquisa
<a href="https://foldoc.org/boolean">booleana</a>
pode ser o que você precisa. Você tem como opções <em>AND</em> (E), <em>OR</em>
(OU), <em>NOT</em> (NÃO) ou uma combinação dessas três. Note que esses
operadores devem ser digitados em letras maiúsculas para que sejam reconhecidos.

<P><B>AND</B> retornará resultados em que ambas as palavras estejam na
página. Por exemplo, "gcc AND patch" vai achar quaisquer páginas que contenham
tanto "gcc" quanto "patch". Este exemplo retorna os mesmos resultados que
"gcc patch", mas definir o operador AND pode ser útil quando combinado com
outros operadores.

<P><B>OR</B> retornará resultados em que uma ou outra palavra, ou ambas,
estejam na página. Por exemplo, "gcc OR patch" vai achar quaisquer páginas que
contenham "gcc" ou "patch" (ou ambas).

<P><B>NOT</B> exclui uma palavra dos resultados.
Por exemplo, "gcc NOT patch" vai achar todas as páginas que contenham "gcc"
e que não contenham "patch". Você também pode digitar "gcc AND NOT patch" para o
mesmo resultado, mas pesquisar utilizando apenas "NOT patch" não é suportado.

<P><B>(</B>...<B>)</B> pode ser usado para agrupar blocos de expressões.
Por exemplo, "(gcc OR make) NOT patch" encontrará todas as páginas que contenham
"gcc" ou "make", mas que não contenham "patch".

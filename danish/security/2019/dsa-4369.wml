#use wml::debian::translation-check translation="8a1c0e346cc4b60809eb2067ebcb114fe8cc027d" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder er opdaget i hypervisor'en Xen:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19961">CVE-2018-19961</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-19962">CVE-2018-19962</a>

    <p>Paul Durrant opdagede at ukorrekt TLB-håndtering kunne medføre 
    lammelsesangreb, rettighedsforøgelse eller informationslækager.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19965">CVE-2018-19965</a>

    <p>Matthew Daley opdagede at ukorrekt håndtering af INVPCID-instruktionen 
    kunne medføre lammelsesangreb foretaget af PV-gæster.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19966">CVE-2018-19966</a>

    <p>Man opdagede at en regression i rettelsen af 
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-15595">CVE-2017-15595</a>
    kunne medføre lammelsesangreb, rettighedsforøgelse eller informationslækager 
    foretaget af en PV-gæst.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19967">CVE-2018-19967</a>

    <p>Man opdagede at en fejl i nogle Intel CPU'er kunne medføre 
    lammelsesangreb foretaget af en gæsteinstans.</p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 4.8.5+shim4.10.2+xsa282-1+deb9u11.</p>

<p>Vi anbefaler at du opgraderer dine xen-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende xen, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4369.data"

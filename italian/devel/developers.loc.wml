#use wml::debian::template title="Distribuzione degli sviluppatori"
#use wml::debian::translation-check translation="6c996020eca890cac2a0243d4633d9bfa7935674" maintainer="Giuseppe Sacco"

<p>Parecchia gente ha espresso il desiderio di conoscere la disposizione
geografica degli sviluppatori Debian. Per questo abbiamo deciso di aggiungere
alla base di dati degli sviluppatori un campo nel quali gli sviluppatori
possono inserire le loro coordinate spaziali.

<p>La mappa sottostante è stata generata con una lista anonima
<a href="developers.coords">delle coordinate degli sviluppatori</a>
usando il programma
<a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.

<p><img src="developers.map.jpeg" alt="Mappa mondiale">

<p>Se sei uno sviluppatore e vuoi aggiungere le tue coordiante nella base di
dati, collegati sul <a href="https://db.debian.org">database degli
sviluppatori Debian</a> e modifica i tuoi dati. Se non conosci le coordinate
della tua città potrai di certo trovarle tramite questi collegamenti:
<ul>
<li><a href="https://osm.org">Openstreetmap</a> È possibile cercare la
propria città nella barra di ricerca. Selezionare le freccie per le
indicazioni accanto alla barra di ricerca. Poi spostare l'indicatore verde
sulla mappa. Le coordinate appariranno nella casella &quot;da&quot;.</li>
</ul>

<p>Il formato per le coordinate è uno dei seguenti:
<dl>
<dt>Gradi decimali
<dd>Il formato è +-DDD.DDDDDDDDDDDDDDD. Questo è il formato utilizzato
    da programmi quali xearth e da molti siti web. Comunque
    la precisione è in genere limitata a 4 o 5 decimali.
<dt>Gradi minuti (DGM)
<dd>Il formato è +-DDDMM.MMMMMMMMMMMMM. Non è un tipo aritmentico, ma
    una rappresentazione compatta di due unità distinte, gradi e
    minuti. Questo output è comune tra alcuni GPS portatili ed è
    utilizzato nel formato NMEA di messaggi GPS.
<dt>Gradi minuti secondi (DGMS)
<dd>Il formato è +-DDDMMSS.SSSSSSSSSSS. Come per DGM, non si tratta
    di un tipo aritmentico ma di una rappresentazione compatta di tre
    diverse unità: gradi, minuti e secondi. Questo output è utilizzato
    in alcuni siti web che danno tre diversi valori per ogni posizione.
    Ad esempio  34:50:12.24523 Nord è rappresentato in DGMS come
    +0345012.24523.
</dl>

<p> 
Per la latitudine + è il Nord, per la longitudine + è l'Est. &Egrave;
importante specificare tutti gli zeri iniziali per evitare le
ambiguità sul formato nel caso che la tua posizione sia minore
di due gradi dal punto zero.

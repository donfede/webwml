#use wml::debian::template title="Le persone: chi siamo e cosa facciamo"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ac8343f61b83fe988dbcb035d77af5bf09ba0709"

# translators: some text is taken from /intro/about.wml

<h2>Sviluppatori e collaboratori</h2>
<p>Debian è prodotta da almeno un migliaio di
sviluppatori da tutto il
<a href="$(DEVEL)/developers.loc">mondo</a> che si
offrono volontari nel loro tempo libero.
Pochi si sono davvero incontrati di persona.
La comunicazione è effettuata principalmente attraverso posta
elettronica (liste di messaggi su lists.debian.org) e IRC (il canale #debian
su irc.debian.org).
</p>

<p>L'elenco completo dei membri ufficiali di Debian può essere trovato
su <a href="https://nm.debian.org/members">nm.debian.org</a>, dove viene
gestita l'associazione. Un più ampio elenco dei collaboratori esterni a
Debian può essere trovato su <a
href="https://contributors.debian.org">contributors.debian.org</a>.</p>
<p>Il Progetto Debian ha un'articolata <a href="organization">struttura
interna</a>. Per maggiori informazioni su come Debian sia strutturata si
guardi la sezione <a href="$(DEVEL)/">l'angolo degli sviluppatori</a>.</p>

<h3><a name="history">Come è cominciato tutto?</a></h3>

<p>Debian nacque nell'agosto 1993 da Ian Murdock come una nuova distribuzione
che potesse finalmente essere veramente aperta, nello spirito di Linux e GNU.
Debian fu ideata per essere poi definita in maniera coscienziosa e mantenuta
con molta cura. È stata molto ristretta all'inizio, formata solo da uno
stretto gruppo di hacker del software libero; gradualmente è poi
cresciuta fino a diventare una grande e organizzata comunità di
sviluppatori e utenti. Vedi <a href="$(DOC)/manuals/project-history/">la
storia in dettaglio</a>.

<p>Visto che molta gente lo ha chiesto, Debian si pronuncia /&#712;de.bi.&#601;n/.
Deriva dal nome del suo ideatore, Ian Murdock, e di sua moglie, Debra.
</p>
  
<h2>Individui e organizzazioni che supportano Debian</h2>

<p>Molti altri individui e organizzazioni fanno parte della comunità Debian:
<ul>
  <li><a href="https://db.debian.org/machines.cgi">Sponsor di hosting e hardware</a></li>
  <li><a href="../mirror/sponsors">Sponsor dei mirror</a></li>
  <li><a href="../partners/">Development and service partners</a></li>
  <li><a href="../consultants">Consulenti</a></li>
  <li><a href="../CD/vendors">Rivenditori dei supporti per l'installazione</a></li>
  <li><a href="../distrib/pre-installed">Rivenditori di computer con Debian pre-installato</a></li>
  <li><a href="../events/merchandise">Rivenditori di materiale promozionale</a></li>
</ul>

<h2><a name="users">Chi utilizza Debian?</a></h2>

<p>Sebbene non siano disponibili statistiche precise (dal momento che
Debian non richiede agli utenti di registrarsi), l'evidenza è abbastanza
forte che Debian è utilizzato da un'ampia gamma di organizzazioni, grandi
e piccole, così come molte migliaia di individui. Vedere la nostra pagina
<a href="../users/">Chi usa Debian?</a> per un elenco di organizzazioni
di alto profilo che hanno presentato brevi presentazioni di come e perché
usano Debian.
</p>

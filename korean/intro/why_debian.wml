#use wml::debian::template title="왜 데비안을 선택하는가" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.

<p>여러 이유로 사용자는 데비안을 그들의 운영체제로 선택합니다.</p>

<h1>주요 이유</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>데비안은 자유 소프트웨어 입니다.</strong></dt>
  <dd>
  데비안은 자유 오픈 소스 소프트웨어로 만들며 항상 100% <a href="free">free</a>입니다. 
  누구나 사용, 수정 및 배포할 수 있습니다.
  이것이 <a href="../users">사용자</a>에게 약속하는 주요 사항입니다.
  가격도 무료입니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 안정적이고 안전한 리눅스 기반 운영체제입니다.</strong></dt>
  <dd>
  데비안은 랩톱, 데스크톱 및 서버를 비롯한 광범위한 장치를 위한 운영 체제입니다. 
  사용자들은 1993년 이후로 안정성과 신뢰성을 좋아했습니다. 모든 패키지에 대해 적절한 기본 구성을 제공합니다. 
  데비안 개발자는 가능한 한 평생 동안 모든 패키지에 대한 보안 업데이트를 제공합니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 넓은 하드웨어를 지원합니다.</strong></dt>
  <dd>
  대부분의 하드웨어를 리눅스 커널에서 이미 지원합니다. 
  하드웨어 전용 드라이버는 무료 소프트웨어가 충분하지 않을 때 사용할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 부드러운 업그레이드를 지원합니다.</strong></dt>
  <dd>
  데비안은 릴리스 주기 내에 쉽고 매끄럽게 업그레이드되는 것으로 잘 알려져 있으며, 
  다음 주요 릴리스로 업그레이드할 수도 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 다른 많은 배포판의 씨앗이며 기반입니다.</strong></dt>
  <dd>
  Ubuntu, Knoppix, PureOS, SteamOS 또는 Tails 같이 가장 많이 사용되는 리눅스 배포판 중 상당수는 소프트웨어 기반으로 데비안을 선택합니다. 
  데비안은 모든 도구를 제공하므로 누구든지 필요에 따라 데비안 아카이브의 소프트웨어 패키지를 자신의 패키지로 확장할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안 프로젝트는 커뮤니티입니다.</strong></dt>
  <dd>
  데비안은 리눅스 운영체제일 뿐만이 아닙니다. 
이 소프트웨어는 전 세계 수백 명의 자원봉사자가 함께 만들었습니다. 
프로그래머나 sysadmin이 아니더라도 데비안 커뮤니티 일원이 될 수 있습니다. 
데비안은 공동체적이고 합의 지향적이며 <a href="../devel/constitution">민주적인 통치 구조</a>를 가집니다. 
모든 데비안의 개발자들은 동등한 권리를 가지고 있기 때문에 그것을 단일 회사가 통제할 수 없습니다. 
60개 이상의 국가에 개발자가 있으며 80개 이상의 언어로 데비안 설치관리자 번역을 지원합니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 여러 설치 옵션이 있습니다.</strong></dt>
  <dd>
    End users will use our
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>
    including the easy-to-use Calamares installer needing very little input or
    prior knowledge. More experienced users can use our unique full featured
    installer while experts can fine tune the installation or even use an
    automated network installation tool.
  </dd>
</dl>

<br>

<h1>기업환경</h1>

<p>
  If you need Debian in a professional environment, you may enjoy these
  additional benefits:
</p>

<dl>
  <dt><strong>데비안은 믿을 수 있습니다.</strong></dt>
  <dd>
    Debian proves its reliability every day in thousands of real world
    scenarios ranging from a single user laptop up to super colliders, stock
    exchanges and the automotive industry. It's also popular in the academic
    world, in science and the public sector.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 전문가가 많습니다.</strong></dt>
  <dd>
    Our package maintainers do not only take care of the Debian packaging and
    incorporating new upstream versions. Often they're experts in the upstream
    software and contribute to upstream development directly. Sometimes they
    are also part of upstream.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 안전합니다.</strong></dt>
  <dd>
    Debian has security support for its stable releases. Many other
    distributions and security researchers rely on Debian's security tracker.
  </dd>
</dl>

<dl>
  <dt><strong>장기 지원.</strong></dt>
  <dd>
    There's <a href="https://wiki.debian.org/LTS">Long Term Support</a>
    (LTS) at no charge. This brings you extended support for the stable
    release for 5 years and more. Besides there's also the
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a> initiative
    that extends supports for a limited set of packages for more than 5 years.
  </dd>
</dl>

<dl>
  <dt><strong>클라우드 이미지.</strong></dt>
  <dd>
    Official cloud images are available for all major cloud platforms. We also
    provide the tools and configuration so you can build your own customized
    cloud image. You can also use Debian in virtual machines on the desktop or
    in a container.
  </dd>
</dl>

<br>

<h1>개발자</h1>
<p>Debian is widely used by every kind of software and hardware developers.</p>

<dl>
  <dt><strong>Public available bug tracking system.</strong></dt>
  <dd>
    Our Debian <a href="../Bugs">Bug tracking system</a> (BTS) is publicly
    available for everybody via a web browser. We do not hide our software
    bugs and you can easily submit new bug reports.
  </dd>
</dl>

<dl>
  <dt><strong>IoT 및 임베디드 장비.</strong></dt>
  <dd>
    We support a wide range of devices like the Raspberry Pi, variants of QNAP,
    mobile devices, home routers and a lot of Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>다중 하드웨어 아키텍처.</strong></dt>
  <dd>
    Support for a <a href="../ports">long list</a> of CPU architectures
    including amd64, i386, multiple versions of ARM and MIPS, POWER7, POWER8,
    IBM System z, RISC-V. Debian is also available for older and specific
    niche architectures.
  </dd>
</dl>

<dl>
  <dt><strong>수많은 소프트웨어 사용 가능.</strong></dt>
  <dd>
    Debian has the largest number of installed packages available
    (currently <packages_in_stable>). Our packages use the deb format which is well
    known for its high quality.
  </dd>
</dl>

<dl>
  <dt><strong>다른 릴리스 선택.</strong></dt>
  <dd>
    Besides our stable release, you get the newest versions of software
    by using the testing or unstable releases.
  </dd>
</dl>

<dl>
  <dt><strong>개발자 도구 및 정책 도움으로 고품질.</strong></dt>
  <dd>
    Several developer tools help to keep the quality to a high standard and
    our <a href="../doc/debian-policy/">policy</a> defines the technical
    requirements that each package must satisfy to be included in the
    distribution. Our continuous integration is running the autopkgtest
    software, piuparts is our installation, upgrading, and removal testing
    tool and lintian is a comprehensive package checker for Debian packages.
  </dd>
</dl>

<br>

<h1>사용자들이 말하는 것</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      For me it's the perfect level of ease of use and stability. I've used
      various different distributions over the years but Debian is the only one
      that just works.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Rock solid. Loads of packages. Excellent community.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian for me is symbol of stability and ease to use.
    </strong></q>
  </li>
</ul>

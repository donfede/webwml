<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several memory leaks were discovered in proftpd-dfsg, a versatile,
virtual-hosting FTP daemon, when mod_facl or mod_sftp
is used which could lead to memory exhaustion and a denial-of-service.</p>

<p>The update also makes automatic upgrades of proftpd-dfsg from Debian 8
to Debian 9 possible again.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.3.5e+r1.3.5b-4+deb9u1.</p>

<p>We recommend that you upgrade your proftpd-dfsg packages.</p>

<p>For the detailed security status of proftpd-dfsg please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/proftpd-dfsg">https://security-tracker.debian.org/tracker/proftpd-dfsg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2338.data"
# $Id: $

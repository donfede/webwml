<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A privilege escalation vulnerability vulnerability was discovered in
<a href="http://net-snmp.sourceforge.net/">Net-SNMP</a>, a set of tools for
collecting and organising information about devices on computer networks.</p>

<p>Upstream notes that:</p>

<ul>
<li>It is still possible to enable this MIB via the <tt>--with-mib-modules</tt>
configure option.</li>

<li>Another MIB that provides similar functionality, namely
<tt>ucd-snmp/extensible</tt>, is disabled by default.</li>

<li>The security risk of <tt>ucd-snmp/pass</tt> and
<tt>ucd-snmp/pass_persist</tt> is lower since these modules only introduce a
security risk if the invoked scripts are exploitable.</li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
5.7.3+dfsg-1.7+deb9u2.</p>

<p>We recommend that you upgrade your net-snmp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2299.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Some vulnerabilities were discovered in ghostscript, an interpreter for the
PostScript language and for PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19134">CVE-2018-19134</a>

    <p>The setpattern operator did not properly validate certain types. A specially
    crafted PostScript document could exploit this to crash Ghostscript or,
    possibly, execute arbitrary code in the context of the Ghostscript process.
    This is a type confusion issue because of failure to check whether the
    Implementation of a pattern dictionary was a structure type.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19478">CVE-2018-19478</a>

    <p>Attempting to open a carefully crafted PDF file results in long-running
    computation. A sufficiently bad page tree can lead to us taking significant
    amounts of time when checking the tree for recursion.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
9.06~dfsg-2+deb8u13.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1620.data"
# $Id: $

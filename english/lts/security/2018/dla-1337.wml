<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in the rubygems package management
framework, embedded in JRuby, a pure-Java implementation of the Ruby
programming language.  </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000075">CVE-2018-1000075</a>

    <p>A negative size vulnerability in ruby gem package tar header that could
    cause an infinite loop.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000076">CVE-2018-1000076</a>

    <p>Ruby gems package improperly verifies cryptographic signatures. A mis-signed
    gem could be installed if the tarball contains multiple gem signatures.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000077">CVE-2018-1000077</a>

    <p>An improper input validation vulnerability in ruby gems specification
    homepage attribute could allow malicious gem to set an invalid homepage
    URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000078">CVE-2018-1000078</a>

    <p>Cross Site Scripting (XSS) vulnerability in gem server display of homepage
    attribute</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.5.6-5+deb7u1.</p>

<p>We recommend that you upgrade your jruby packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1337.data"
# $Id: $

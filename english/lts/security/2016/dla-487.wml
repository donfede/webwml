<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Debian Long Term Support (LTS) Team is unable to continue supporting
different packages in the extended life cycle of Wheezy LTS. The
debian-security-support package provides the check-support-status tool that
helps to warn the administrator about installed packages whose security support
is limited or has to prematurely end.</p>

<p>debian-security-support version 2016.05.24~deb7u1 updates the list of packages
with restricted support in Wheezy LTS, adding the following:</p>

<table>
<tr><th>Source Package</th>	<th>Last supported version</th>	<th>EOL date</th>	<th>Additional information</th></tr>
<tr><td>libv8</td>		<td>3.8.9.20-2</td>		<td>2016-02-06</td>	<td><a href="https://lists.debian.org/debian-lts/2015/08/msg00035.html">https://lists.debian.org/debian-lts/2015/08/msg00035.html</a></td></tr>
<tr><td>mediawiki</td>		<td>1:1.19.20+dfsg-0+deb7u3</td><td>2016-04-26</td>	<td><a href="https://www.debian.org/releases/jessie/amd64/release-notes/ch-information.html#mediawiki-security">https://www.debian.org/releases/jessie/amd64/release-notes/ch-information.html#mediawiki-security</a></td></tr>
<tr><td>sogo</td>		<td>1.3.16-1</td>		<td>2016-05-19</td>	<td><a href="https://lists.debian.org/debian-lts/2016/05/msg00197.html">https://lists.debian.org/debian-lts/2016/05/msg00197.html</a></td></tr>
<tr><td>vlc</td>		<td>2.0.3-5+deb7u2</td>		<td>2016-02-06</td>	<td><a href="https://lists.debian.org/debian-lts/2015/11/msg00049.html">https://lists.debian.org/debian-lts/2015/11/msg00049.html</a></td></tr>
</table>

<p>If you rely on those packages on a system running Debian 7 <q>Wheezy</q>, we
recommend you to upgrade to Debian 8 <q>Jessie</q>, the current stable release.
Note however that the mediawiki support has also ended in Jessie. </p>

<p>We recommend you to install the debian-security-support package to verify the
support status of the packages installed on the system.</p>

<p>More information about Debian LTS can be found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in debian-security-support version 2016.05.24~deb7u1</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-487.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Dawid Golunski from legalhackers-com [1] discovered that the mail
transport in Swift Mailer allowed remote attackers to pass extra
parameters to the mail command and consequently execute arbitrary code
via a \" (backslash double quote) in a crafted e-mail address in the
From, ReturnPath, or Sender header.</p>

<p>[1] <url "https://legalhackers.com/advisories/SwiftMailer-Exploit-Remote-Code-Exec-CVE-2016-10074-Vuln.html"></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.5-1+deb7u1.</p>

<p>We recommend that you upgrade your libphp-swiftmailer packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-792.data"
# $Id: $

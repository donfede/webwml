<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9263">CVE-2016-9263</a>

    <p>When domain-based flashmediaelement.swf sandboxing is not used,
    allows remote attackers to conduct cross-domain Flash injection
    (XSF) attacks by leveraging code contained within the
    wp-includes/js/mediaelement/flashmediaelement.swf file.</p>

    <p>This issue was resolved by completely removing
    flashmediaelement.swf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14718">CVE-2017-14718</a>

    <p>WordPress was susceptible to a Cross-Site Scripting attack in the
    link modal via a javascript: or data: URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14719">CVE-2017-14719</a>

    <p>WordPress was vulnerable to a directory traversal attack during
    unzip operations in the ZipArchive and PclZip components.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14720">CVE-2017-14720</a>

    <p>WordPress allowed a Cross-Site scripting attack in the template list
    view via a crafted template name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14721">CVE-2017-14721</a>

    <p>WordPress allowed Cross-Site scripting in the plugin editor via a
    crafted plugin name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14722">CVE-2017-14722</a>

    <p>WordPress allowed a Directory Traversal attack in the Customizer
    component via a crafted theme filename.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14723">CVE-2017-14723</a>

    <p>WordPress mishandled % characters and additional placeholder values
    in $wpdb->prepare, and thus did not properly address the possibility
    of plugins and themes enabling SQL injection attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14725">CVE-2017-14725</a>

    <p>WordPress was susceptible to an open redirect attack in
    wp-admin/user-edit.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14990">CVE-2017-14990</a>

    <p>WordPress stores cleartext wp_signups.activation_key values (but
    stores the analogous wp_users.user_activation_key values as hashes),
    which might make it easier for remote attackers to hijack
    unactivated user accounts by leveraging database read access
   (such as access gained through an unspecified SQL injection
    vulnerability).</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u17.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1151.data"
# $Id: $

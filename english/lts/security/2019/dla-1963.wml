<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two buffer allocation issues were identified in poppler.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9959">CVE-2019-9959</a>

    <p>An unexpected negative length value can cause an integer
    overflow, which in turn making it possible to allocate a large
    memory chunk on the heap with size controlled by an attacker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10871">CVE-2019-10871</a>

    <p>The RGB data are considered CMYK data and hence it reads 4 bytes
    instead of 3 bytes at the end of the image. The fixed version
    defines SPLASH_CMYK which is the upstream recommended solution.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.26.5-2+deb8u12.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1963.data"
# $Id: $

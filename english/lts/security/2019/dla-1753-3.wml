<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of proftpd-dfsg issued as DLA-1753-1 caused a regression
when the creation of a directory failed during sftp transfer. The sftp
session would be terminated instead of failing gracefully due to a
non-existing debug logging function.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.5e+r1.3.5-2+deb8u2.</p>

<p>We recommend that you upgrade your proftpd-dfsg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1753-3.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in libvorbis, a decoder library for Vorbis
General Audio Compression Codec.</p>

<p>2017-14633</p>

      <p>In Xiph.Org libvorbis 1.3.5, an out-of-bounds array read
      vulnerability exists in the function mapping0_forward() in
      mapping0.c, which may lead to DoS when operating on a crafted
      audio file with vorbis_analysis().</p>

<p>2017-11333</p>

      <p>The vorbis_analysis_wrote function in lib/block.c in Xiph.Org
      libvorbis 1.3.5 allows remote attackers to cause a denial of
      service (OOM) via a crafted wav file.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.4-2+deb8u3.</p>

<p>We recommend that you upgrade your libvorbis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2039.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>DLA-1789-1 shipped updated CPU microcode for most types of Intel CPUs as
mitigations for the MSBDS, MFBDS, MLPDS and MDSUM hardware vulnerabilities.</p>

<p>This update provides additional support for some Sandybridge server
and Core-X CPUs which were not covered in the original May microcode
release. For a list of specific CPU models now supported please refer
to the entries listed under CPUID 206D6 and 206D7 at
<a href="https://www.intel.com/content/dam/www/public/us/en/documents/corporate-information/SA00233-microcode-update-guidance_05132019.pdf">https://www.intel.com/content/dam/www/public/us/en/documents/corporate-information/SA00233-microcode-update-guidance_05132019.pdf</a></p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.20190618.1~deb8u1 of the intel-microcode package.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be found
at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For the detailed security status of intel-microcode please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1789-2.data"
# $Id: $

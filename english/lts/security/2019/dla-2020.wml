<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in the Oniguruma regular
expressions library, notably used in PHP mbstring.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19012">CVE-2019-19012</a>

    <p>An integer overflow in the search_in_range function in regexec.c
    leads to an out-of-bounds read, in which the offset of this read
    is under the control of an attacker. (This only affects the 32-bit
    compiled version). Remote attackers can cause a denial-of-service
    or information disclosure, or possibly have unspecified other
    impact, via a crafted regular expression.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19204">CVE-2019-19204</a>

    <p>In the function fetch_range_quantifier in regparse.c, PFETCH is
    called without checking PEND. This leads to a heap-based buffer
    over-read and lead to denial-of-service via a crafted regular
    expression.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19246">CVE-2019-19246</a>

    <p>Heap-based buffer over-read in str_lower_case_match in regexec.c
    can lead to denial-of-service via a crafted regular expression.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.9.5-3.2+deb8u4.</p>

<p>We recommend that you upgrade your libonig packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2020.data"
# $Id: $
